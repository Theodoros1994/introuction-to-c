// deixtes
int main(int argc, char *argv[]) {
 int x = 5, *p;

 p = &x;
 printf("x = %d\n",x);
 printf("&x = %p\n", &x);
 printf("p = %p\n");
 printf("*p = %d\n",*p);

 *p = *p + 10;

 printf("x = %d\n",x);
 return 0;
}

/* 
To %p σημαίνει διεύθυνση
δείκτη και αν στη θέση του
χρησιμοποιούσαμε το %d
τότε θα εμφανιζόταν το
6422184 που είναι η
δεκαδική αναπαράσταση του
δεκαεξαδικού 0061FEA8
*/




// vgazoun dieuthinsi
printf("&x = %p\n", &x);
 printf("p = %p\n");

// vgazoun apotelesma

printf("*p = %d\n",*p);

// logiko na auxanei kai to apotelesma +10 kai meta ektiposi

*p = *p + 10;
printf("x = %d\n",x);


/*
Ένας πίνακας δεν μπορεί να πάρει
σαν τιμή έναν δείκτη ούτε τη
διεύθυνση ενός άλλου πίνακα

*/

// min function
int MyMin(int k, int m) {
int minTimi;
 if(k < m){
 minTimi = k;
 }
 else {
 minTimi = m;
 }
 return(minTimi);
}




int main(int argc, char *argv[]){
int min, a, b;
scanf("%d",&a);
scanf("%d",&b);
min = MyMin(a,b);
min = MyMin(10, 20);
min = MyMin(10, MyMin(a,b));
printf("min = %d", min);
return 0;
}


// function me pointer

void refMyMin(int k, int m, int *minTimi) {
if(k < m){
 *minTimi = k;
}
else {
 *minTimi = m;
}
}
int main(int argc, char *argv[]){
int min, a, b;
a = 3; b = 5;
refMyMin(a,b, &min);
printf("Min = %d", min);
}


// swap me deiktes

void swap(int *k, int *m) {
int temp;
temp = *k;
*k = *m;
*m = temp;
}
nt main(int argc, char *argv[]){
int a, b;
a = 3; b = 5;
swap(&a,&b);
}


/*

Να δημιουργήσετε μια συνάρτηση με το όνομα power, η οποία θα
δέχεται σαν ορίσματα 1 πραγματικό αριθμό, έστω a και ένα ακέραιο
έστω b, και θα επιστρέφει την δύναμη τους, δηλαδή το a υψωμένο στην
δύναμη του b.


Να δημιουργήσετε μια συνάρτηση με το όνομα abs, η οποία θα δέχεται
σαν ορίσμα 1 ακέραιο αριθμό, έστω a, και θα επιστρέφει την απόλυτη
τιμή του.

Να δημιουργήσετε μια συνάρτηση με το όνομα poly, η οποία θα
υπολογίζει την τιμή του πολυωνύμου ax^2+bx+c



float power(float a, int b){
 int i;
 float apot = 1.0;

 for(i = 1; i <= b; i++) {
 apot = apot * a;
 }
 return apot;
}

int abs(int a){
 if(a < 0)
 return(a * -1);
 return a;
}

float poly(float a, float b, float c, float x){
 return (a * power(x, 2)) + (b * x) + c ;
}

 */

// Πίνακες σαν παράμετροι σε συναρτήσεις //


int *Pin or int Pin[]


/* 
Η αρχή ενός πίνακα ακεραίων χωρίς συγκεκριμένη διάσταση. Θα
μπορούσαμε να το γράψουμε και int *X

 */



int sumPin(int X[], int n) //  Το πλήθος των στοιχείων του πίνακα

{
 int i, sum = 0;
 for(i = 0; i < n; i++)
 {
 sum = sum + X[i];
 }
 return (sum);
}

int main(int argc, char *argv[]) {
 int A[3] = { 4, 5, 6};
 int K[5] = { 6, 9, 2, 3, 4};
 printf("%d\n", sumPin(A, 3));
 printf("%d\n", sumPin(K, 5));
}
 /*
 Δημιουργούμε δύο πίνακες με
διαφορετικό πλήθος στοιχείων και
ονόματα Α και Κ αντίστοιχα. Καλούμε
την sumPin με ορίσματα Α και 3. Σε
αυτή την περίπτωση το Χ δείχνει στην
αρχή του Α και το n είναι 3. Στην
δεύτερη περίπτωση το Χ δείχνει στην
αρχή του Κ και το n είναι 5.
 
 
 */


/* 


δέχονται σαν όρισμα την διεύθυνση
ενός πίνακα ή πιο συγκεκριμένα την διεύθυνση του πρώτου στοιχείου. Στην main
καλούμε αρχικά την συνάρτηση με όρισμα την διεύθυνση του πίνακα test και έπειτα
με την διεύθυνση του pinB. Για αυτό το λόγο περνάμε σαν δεύτερο όρισμα το μήκος
του πίνακα, ώστε ξεκινώντας από την αρχική διεύθυνση να διασχίσουμε το
συγκεκριμένο πλήθος τιμών


  */

int SmallValue(int *pinakas, int no) {
int i, min;
 min = *pinakas;
 for (i = 1; i < no; i++)
 if(*(pinakas+i) < min)
 min = *(pinakas+i);
 return min;
}

int main(int argc, char *argv[]) {
 int test[5] = {4, 2, 6, 8, 9};
 int pinB[3] = {3, 8, 9};
printf("%d\n",SmallValue(test, 5));
 printf("%d\n",SmallValue(pinB, 3));
return 0;
}

/*  
ASKISARA
Να δημιουργήσετε τέσσερις συναρτήσεις με τα ονόματα fillArray,
sortArray, showArray και findPos αντίστοιχα. Oι τρεις συναρτήσεις θα
δέχονται δύο ορίσματα : α) ένα δείκτη σε ένα πίνακα ακεραίων και β)
έναν ακέραιο αριθμό που αντιστοιχεί στο πλήθος των στοιχείων του
πίνακα. Η fillArray θα γεμίζει τις αντίστοιχες θέσεις του πίνακα με
τιμές από το πληκτρολόγιο. Η sortArray θα ταξινομεί την πίνακα και
η showArray θα εκτυπώνει τα στοιχεία του πίνακα. H findPos θα
δέχεται τα ίδια ορίσματα και επιπλέον ένα ακέραιο αριθμό, του οποίου
θα αναζητά την θέση μέσα στο πίνακα των ακεραίων. Αν δεν υπάρχει
ο ακέραιος αριθμός τότε η findPos θα επιστρέφει την τιμή -1.
Στην συνάρτηση main να δηλώσετε ένα πίνακα ακεραίων με
χωρητικότητα 5 θέσεων και με την βοήθεια των τριών πρώτων
συναρτήσεων να το γεμίσετε με αριθμούς που θα δώσετε από το
πληκτρολόγιο, να τον ταξινομήσετε και να εκτυπώσετε τα στοιχεία
του. Τέλος χρησιμοποιήσετε την findPos για να αναζητήσετε την θέση
μέσα στο πίνακα κάποιου ακεραίου αριθμού που θα διαβάσετε από το
πληκτρολόγιο.

*/

void fillArray(int *A, int n) {
 int i;
 for(i = 0; i < n; i++)
 scanf("%d", &A[i]);
}
void showArray(int *A, int n) {
 int i;
 for(i = 0; i < n; i++)
 printf("%d\t",A[i]);

 printf("\n");
}
void swap(int *a, int *b) {
 int temp;
 temp = *a;
 *a = *b;
 *b = temp;
}
void sortArray(int *A, int n) {
 int i, j;

 for(j = 0; j < n; j++)
 for(i = 0; i < (n - 1); i++)
 if(A[i] > A[i+1])
 swap(&A[i], &A[i+1]);
}
int findPos(int *A, int n, int X) {
 int i, pos = -1;
 for(i = 0; i < n; i++)
 if(A[i] == X)
 {
 pos = i;
 break;
 }

 return (pos);
}
int main(int argc, char *argv[]) {
 int APin[3], no, pos;
 fillArray(APin, 3);
 sortArray(APin, 3);
 showArray(APin, 3);

 printf("Type an integer number : ");
 scanf("%d", &no);
 pos = findPos(APin, 3, no);
 if(pos < 0)
 printf("Not found\n");
 else
 printf("Found at position : %d\n", pos);

 return 0;
}









/*

Να δημιουργήσετε μια συνάρτηση με το όνομα calcPins, η οποία
δέχεται δύο πίνακες πραγματικών με ίσο πλήθος στοιχείων έστω Χ
και Υ, και το πλήθος των στοιχείων του πίνακα, έστω n και επιστρέφει
Παράδειγμα
Στην συνάρτηση main να δημιουργήσετε δύο πίνακες πραγματικών με
χωρητικότητα 3 θέσεων

 */




float calcPins(float X[], float Y[], int n) {
 int i;
 float sum = 0;
 for(i = 0; i < n; i++)
 {
 sum = sum + ((X[i] + Y[i]) / (X[i] * Y[i]));
 }
 return(sum);
}
int main(int argc, char *argv[]) {
 float A[3], B[3];
 int i;
 for(i=0; i < 3; i++) {
 scanf("%f", &A[i]);
 scanf("%f", &B[i]);
}
 printf("%f", calcPins(A, B, 3));
 return 0;
}


// Πίνακες 3-διαστάσεων σαν ορίσματ


void ShowValues3D(int rows, int cols, int height, int pin[][cols][height]) {
int i, j, k;
for(i=0; i < rows; i++) {
 for(j=0; j < cols; j++) {
 for(k=0; k < height; k++) {
 printf("%d\t", pin[i][j][k]);
 }
 printf("\n");
 }
 printf("\n");
}
}
int main(int argc, char *argv[]) {
int B[2][3][4] = { { {1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4} },
 { {1, 2, 3, 4}, {1, 2, 3, 4}, {1, 2, 3, 4} }
 };
 ShowValues3D(2,3,4, B);
 return 0;
}

// anadromi

/* Να διαβάσετε δύο αριθμούς και να ελέγξετε αν είναι και οι δύο μέσα στο
διάστημα από 0..4. Να εκτυπώνονται αντίστοιχα μηνύματα */

int main(int argc, char *argv[]) {
 int A, B;
 printf("Type two integers : ");
 scanf("%d%d",&A,&B);
 if ((A > 0) && (A < 4)) {
 if((B > 0) && (B < 4)) {
 printf("both numbers belong to range (0..4)\n");
 }
 else {
 printf("A belons to range (0..4) but not B\n");
 }
 }
 else {
 if((B > 0) && (B < 4)) {
 printf("B belons to range (0..4) but not A\n");
 }
 else {
 printf("Neither number belong to range (0..4)\n");
 }
 } return 0;
}


/* 
Και οι δύο αριθμοί εκτός ορίων Περίπτωση 0
Ο πρώτος εντός ο δεύτερος εκτός Περίπτωση 1
Ο πρώτος εκτός ο δεύτερος εντός Περίπτωση 2
Και οι δύο αριθμοί εντός ορίων Περίπτωση 3
Οπότε μπορούμε να αντιστοιχίσουμε ένα ακέραιο αριθμό όπου
ανάλογα την περίπτωση θα επιστρέφει την αντίστοιχη τιμή. */
int checkLimits(int A, int B, int downLimit, int upLimit)
{
 int apot1 = 0, apot2 = 0;

 apot1 = (A > downLimit) && (A < upLimit);

 apot2 = (B > downLimit) && (B < upLimit);

 switch (apot1 + apot2)
 {
 case 0 : return 0; break;
 case 2 : return 3; break;
 case 1 : if(apot1)
 return 1;
 else
 return 2;
 break;
 }
}











int RecursiveParagontiko(int number)
{
 if(number == 1)
 return 1;
 else
 return number * RecursiveParagontiko(number - 1);
}
/* 
Να δημιουργήσετε μια συνάρτηση με το όνομα CalcSum, η οποία θα
δέχεται σαν ορίσματα 1 ακέραιο αριθμό, έστω a, και θα υπολογίζει το
άθροισμα όλων των αριθμών από το 1 μέχρι και το a
 */


int CalcSum(int number)
{
 int I, sum = 0;
 for(i = 1; i <= number; i++)
 sum = sum + i;
 return(sum);
}







