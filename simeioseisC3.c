
// Μαθηματικές συναρτήσεις


RAND_MAX, η οποία στους
περισσότερους μεταγλωττιστές ορίζεται σαν μία σταθερά με τιμή το 32767.



//Αλφαριθμητικά


char address[] = { 'H', 'e', 'l', 'l', 'o', '\0' };

// print string address

char address[10];
 address[0] = 'G'; address[1] = 'o'; address[2] = 'o'; address[3] = 'd';
 address[4] = ' '; address[5] = 'd'; address[6] = 'a'; address[7] = 'y';
 address[8] = '\0';
 printf("%s", address);
 strcpy(address, "The boy");
 printf("%s", address);



void MyStrcpy (char strDest [], char strSource []) {
 int n=0;

 do {
 strDest[n] = strSource[n];
 } while (strSource[n++] != '\0');
}
int main(int argc, char *argv[]) {
 char address[10];
 MyStrcpy(address, "hello");

 printf("%s", address);
 return 0;
}


//string.h


// Δυναμική δέσμευση της μνήμης 

/* Δείκτης = (TύποςΔεδομένων *) malloc (Πλήθος Bytes);
H συνάρτηση malloc δεσμεύει
δυναμικά χώρο στην μνήμη, ικανό να
αποθηκεύσει Πλήθος Bytes που
συνήθως υπολογίζονται από το
γινόμενο (Πλήθος Στοιχείων) *
(Μέγεθος Τύπου δεδομένων)

 */
int *array;
array = (int *) malloc(4 * sizeof(int));
if(array == NULL){
 printf("Not enough memory");
 exit(0);
}
else {
 int i;
 for(i = 0; i < 4; i++)
 scanf("%", &array[i]);
 free(array);
}













/////////////////////////////////////////////////////////




int main(int argc, char *argv[]){
 int *pin; //Η δήλωση του δείκτη για το πίνακα
των ακεραίων
 int pLen, sum = 0;

 printf(" Type number of elements : "); //Το επιθυμητό πλήθος των στοιχείων
του πίνακα. Αυτή η μεταβλητή δεν
αλλάζει τιμή καθ’ όλη την διάρκεια
του προγράμματος.
 scanf("%d", &pLen);

 if((pin = (int *)malloc(pLen * sizeof(int)) == NULL)
 printf("Error in memory allocation\n");
Η δέσμευση της μνήμης για το
συγκεκριμένο πλήθος στοιχείων. Αν
δεν υπάρχει διαθέση μνήμη τότε το
σύστημα επιστρέφει την τιμή NULL.
 else {
 for(int i = 0; i < pinLen; i++)
 scanf("%d", &pin[i]);

 for(int i = 0; i < pinLen ; i++)
 sum += pin[i];

 printf("sum = %d\n" sum);

 free(pin);
 }
return 0;
}




// memory functions //
Θα πρέπει να ΜΗΝ ΑΠΟΔΕΣΜΕΥΣΟΥΜΕ (free) την μνήμη μέσα στην συνάρτηση.



int *readPinaka(int plithos) {
 int *pin;

 if((pin = (int *) malloc(plithos *
sizeof(int)) == NULL) {
 return NULL;
 }
 else
 {
 for(int i = 0; i < plithos; i++)
 scanf("%d", &pin[i]);
 return pin;
 }
}
int main(int argc, char *argv[]){
 int *pinakas;

 if((pinakas = readPinaka(5)) == NULL)
 {
 printf("Error in memory allocation.");
 exit(0);
 }

 for(int i = 0; i < 5; i++)
 printf("%d\t",pinakas[i]);

 free(pinakas);

 return 0;
}



// memory deixtis

char *myStrCpy(char *source)
{
 char *dupl;
 int i, strPlithos = strlen(source);

 if((dupl = (char *) malloc(strPlithos+1)) == NULL) {
 return NULL;
 }
 else
 {
 for(i = 0; i < strPlithos; i++)
 dupl[i] = source[i];

 dupl[i] = '\0';
 return dupl;
 }
}
int main(int argc, char *argv[]){
 char *copyString;

 if((copyString = nzStrCpy("Hello")) == NULL)
 {
 printf("Error in memory allocation.");
 }

 printf("%s\n", copyString);
 printf("%d\n", strlen(copyString));

 free(copyString);
 return 0;

}
typedef struct {
 int x;
 int y;
} point;
int main(int argc, char *argv[]) {
 dekadikos x = 6.0f;
 WORD w = 12;
 printf("x = %f\n", x);
 printf("w = %u\n", w);
 point A;
 A.x = 22;
 A.y = 5;
 printf("A.x = %d and A.y= %d\n", A.x, A.y);
 return 0;



H δομή περιέχει τους βασικούς
τύπους δεδομένων και τους
ενώνει κάτω από ένα κοινό
όνομα, το Birthday


struct Birthday {
 int day;
 int month;
 int year;
 };
int main(int argc, char *argv[]) {
 struct Birthday myBirthday;

 myBirthday.day = 2;
 myBirthday.month = 4;
 myBirthday.year = 1967;
 printf("%d", myBirthday.day);
 printf("%d", myBirthday.month);
 printf("%d", myBirthday.year);

 return 0;

 // domes me pointer
 
 struct Birthday {
 int day;
 int month;
 int year;
 };

int main(int argc, char *argv[]) {
 struct Birthday myBirthday;
 struct Birthday *pointer;

 pointer = & myBirthday;
 myBirthday.day = 2;
 myBirthday.month = 4;
 myBirthday.year = 1967;

 printf("%d\n",(*pointer).day);
 printf("%d\n",(*pointer).month);
 printf("%d\n",pointer->year);

 return 0;
}//Συγκρίσεις με δομές
int Compare(struct Birthday aDate, struct Birthday bDate) {
 int adays = aDate.year * 365 + getMonthDays(aDate.month) + aDate.day;
 int bdays = bDate.year * 365 + getMonthDays(bDate.month) + bDate.day;
 if(adays > bdays) {
return -1;
 }
 else if(adays < bdays) {
return 1;
 }
 else {
 return 0;
 }
}
int main(int argc, char *argv[]) {
 struct Birthday x = Initialize(23, 9, 2016);
struct Birthday y = Initialize(23, 9, 2016);

 if( Compare(x, y) == 0) {
 printf("The two days are the same.\n");
 }
 return 0;
}